# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import account


def register():
    Pool.register(
        account.Journal,
        account.Payment,
        account.PaymentGroup,
        account.MoveLine,
        module='account_payment_receipt_type', type_='model')
    Pool.register(
        account.PaymentProcessing,
        module='account_payment_receipt_type', type_='model',
        depends=['account_payment_processing'])
    Pool.register(
        account.PrintReceipt,
        module='account_payment_receipt_type', type_='wizard')
