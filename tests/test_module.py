# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class AccountPaymentReceiptTypeTestCase(ModuleTestCase):
    """Test Account Payment Receipt Type module"""
    module = 'account_payment_receipt_type'
    extras = ['account_payment_processing']


del ModuleTestCase
