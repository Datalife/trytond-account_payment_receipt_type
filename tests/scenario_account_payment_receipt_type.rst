=====================================
Account Payment Receipt Type Scenario
=====================================

Imports::

    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences

    >>> today = datetime.datetime.now()

Install modules::

    >>> config = activate_modules(['account_payment_receipt_type', 'account_payment_processing'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')
    >>> period = fiscalyear.periods[0]


Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> receivable = accounts['receivable']
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']
    >>> cash = accounts['cash']


Create processing accounts::

    >>> Account = Model.get('account.account')
    >>> processing = Account()
    >>> processing.name = 'Customers Processing Payments'
    >>> processing.parent = receivable.parent
    >>> processing.type = receivable.type
    >>> processing.reconcile = True
    >>> processing.party_required = True
    >>> processing.deferral = True
    >>> processing.save()


Get journals::

    >>> Journal = Model.get('account.journal')
    >>> journal_revenue, = Journal.find([
    ...         ('code', '=', 'REV'),
    ...         ])
    >>> journal_cash, = Journal.find([
    ...         ('code', '=', 'CASH'),
    ...         ])


Create sequence::

    >>> Sequence = Model.get('ir.sequence')
    >>> SequenceType = Model.get('ir.sequence.type')
    >>> seq_type, = SequenceType.find([('name', '=', 'Payment Journal')])
    >>> sequence_payment_journal = Sequence(
    ...     name='Payment Journal Sequence',
    ...     prefix='RECEIPT-',
    ...     sequence_type=seq_type)
    >>> sequence_payment_journal.save()


Create payment journal::

    >>> PaymentJournal = Model.get('account.payment.journal')
    >>> payment_journal = PaymentJournal(
    ...     name='Processing',
    ...     process_method='manual',
    ...     clearing_journal=journal_cash,
    ...     clearing_account=cash,
    ...     processing_journal=journal_revenue,
    ...     processing_account=processing)
    >>> payment_journal.save()

    >>> payment_journal_sequence = PaymentJournal(
    ...     name='Processing with sequence',
    ...     process_method='manual',
    ...     receipt_sequence=sequence_payment_journal,
    ...     clearing_journal=journal_cash,
    ...     clearing_account=cash,
    ...     processing_journal=journal_revenue,
    ...     processing_account=processing)
    >>> payment_journal_sequence.save()


Create party::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()


Create moves to pay for the customer::

    >>> Move = Model.get('account.move')
    >>> move = Move()
    >>> move.period = period
    >>> move.journal = journal_revenue
    >>> move.date = period.start_date
    >>> line = move.lines.new()
    >>> line.account = revenue
    >>> line.credit = Decimal(100)
    >>> line = move.lines.new()
    >>> line.account = receivable
    >>> line.maturity_date = today
    >>> line.debit = Decimal(100)
    >>> line.party = customer
    >>> move.click('post')

    >>> move2 = Move()
    >>> move2.period = period
    >>> move2.journal = journal_revenue
    >>> move2.date = period.start_date
    >>> line = move2.lines.new()
    >>> line.account = revenue
    >>> line.credit = Decimal(100)
    >>> line = move2.lines.new()
    >>> line.account = receivable
    >>> line.maturity_date = today
    >>> line.debit = Decimal(100)
    >>> line.party = customer
    >>> move2.click('post')


Create customer payments and check receipt numbers::

    >>> Payment = Model.get('account.payment')
    >>> line, = [l for l in move.lines if l.account == receivable]
    >>> pay_line = Wizard('account.move.line.pay', [line])
    >>> pay_line.execute('next_')
    >>> pay_line.form.journal = payment_journal
    >>> pay_line.execute('next_')
    >>> payment, = Payment.find([('state', '=', 'draft')])
    >>> payment.click('submit')
    >>> payment.receipt_number != None
    False
    >>> process_payment = Wizard('account.payment.process', [payment])
    >>> process_payment.execute('process')
    >>> payment.reload()
    >>> payment.processing_move != None
    True
    >>> payment.processing_move.description != None
    False

    >>> line, = [l for l in move2.lines if l.account == receivable]
    >>> pay_line = Wizard('account.move.line.pay', [line])
    >>> pay_line.execute('next_')
    >>> pay_line.form.journal = payment_journal_sequence
    >>> pay_line.execute('next_')
    >>> payment2, = Payment.find([('state', '=', 'draft')])
    >>> payment2.click('submit')
    >>> payment2.receipt_number != None
    True
    >>> payment2.receipt_number
    'RECEIPT-1'
    >>> process_payment = Wizard('account.payment.process', [payment2])
    >>> process_payment.execute('process')
    >>> payment2.reload()
    >>> payment2.processing_move != None
    True
    >>> payment2.processing_move.description != None
    True
    >>> payment2.processing_move.description
    '[RECEIPT-1] '
    >>> len(payment2.processing_move.lines)
    2
    >>> [m.description for m in payment2.processing_move.lines]
    ['[RECEIPT-1] ', '[RECEIPT-1] ']